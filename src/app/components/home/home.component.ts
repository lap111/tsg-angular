import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationService } from '../../services/pagination.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private router:Router,
    private paginationService:PaginationService
  ) { }

  ngOnInit() {
  }

  scrollHandler(e){
    if(this.router.url === '/search'){
      // console.log(e);
      this.paginationService.setPosotion(e);
    }    
  }

}
