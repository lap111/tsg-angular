import { Component, OnInit } from '@angular/core';
import { MaterialService } from '../../services/material.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  page: number =  0;
  material;
  item: number;

  constructor(
    private materialService: MaterialService,
    private authService:AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.authService.handleAuthentication();
    this.material = this.materialService.getMaterial();
  }

  onPageChange(e){
    this.page = e;
  }

  selectItem(i, material){
    this.router.navigate([`/content/${i}`])
  }

}
