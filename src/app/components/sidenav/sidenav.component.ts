import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MaterialService } from '../../services/material.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  simpleCollapsibleItems

  constructor(
    private materialService:MaterialService
  ) { }

  ngOnInit() {
    this.simpleCollapsibleItems = this.materialService.getMaterialFromSidenav()
  }

}
