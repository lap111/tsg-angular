import { Component, OnInit } from '@angular/core';
import { MaterialService } from '../../services/material.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationService } from '../../services/pagination.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  materials = [];
  subscription: any;
  positionTop: boolean = false;
  positionBottom: boolean = false;

  constructor(
    private materialService:MaterialService,
    private paginationService:PaginationService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.paginationService.position
    .subscribe(value => {
      if(value == 'top' && !this.positionTop){
        this.positionTop = true;
        setTimeout(() => {
          this.positionTop = false;
          this.materials = this.materialService.getMaterialPagination(this.materials.length)
        },1000);
      };
      if(value == 'bottom' && !this.positionBottom){
        this.positionBottom = true;
        setTimeout(() => {
          this.positionBottom = false;
          this.materials = this.materialService.getMaterialPagination(this.materials.length)
          
        },1000);
      };
    })
   }

  ngOnInit() {
    this.materials = this.materialService.getMaterialPagination(this.materials)
  }

  selectItemBySearch(i, material){
    this.router.navigate([`/content/${i}`])
  }

}
