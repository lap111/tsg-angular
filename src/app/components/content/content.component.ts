import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterialService } from '../../services/material.service';
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  item;
  material;

  constructor(
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private materialService:MaterialService
  ) { 
    
  }

  ngOnInit() {
    this.item = this.activatedRoute.snapshot.params.id;
    this.material = this.materialService.getCurrentMaterial(this.activatedRoute.snapshot.params.id);
  }

}
