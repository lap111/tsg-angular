import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  profile: any;
  searchField: string = '';

  constructor(
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    console.log('this.profile')
    // if (this.authService.userProfile) {
    //   this.profile = this.authService.userProfile;
    // } else {
    //   this.authService.getProfile((err, profile) => {
    //     this.profile = profile;
    //   });
    // }

  }

  searchMaterial(e) {

    if (e.keyCode == 13 && this.searchField !== '' || e == 'click' && this.searchField !== '') {
      this.router.navigate([`search`])
    }
  }

  authorization() {
    this.authService.login()
  }

}
