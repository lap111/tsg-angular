import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  arr = [
    {
      img: './assets/img/1.jpg',
      title: 'text 1',
      subtitle: 'One more line of text',
      main: {
        text: 
        'The gig economy creates a lot of great benefits; flexible work schedule, on-demand skills, adaptable and more affordable services (without the typical overheads of a firm). Everyday more and more platforms are emerging to help people do this kind of work. They connect people to gigs, streamline the payment and create trust. All great things, right?'+
        'An unprecedented number of independent workers will disrupt the economic landscape. When I say disrupt I don’t mean in the cliché Silicon Valley definition, I mean in the true definition: “To destroy, usually temporarily, the normal continuance or unity…” A shift like this will disrupt incomes and relationships, personal and professional.'
      }
    },
    {
      img: './assets/img/2.jpg',
      title: 'text 2',
      subtitle: 'One more line of text',
      main: {
        text: 
        'The gig economy creates a lot of great benefits; flexible work schedule, on-demand skills, adaptable and more affordable services (without the typical overheads of a firm). Everyday more and more platforms are emerging to help people do this kind of work. They connect people to gigs, streamline the payment and create trust. All great things, right?'+
        'An unprecedented number of independent workers will disrupt the economic landscape. When I say disrupt I don’t mean in the cliché Silicon Valley definition, I mean in the true definition: “To destroy, usually temporarily, the normal continuance or unity…” A shift like this will disrupt incomes and relationships, personal and professional.'
      }
    },
    {
      img: './assets/img/3.jpg',
      title: 'text 3',
      subtitle: 'One more line of text',
      main: {
        text: 
        'The gig economy creates a lot of great benefits; flexible work schedule, on-demand skills, adaptable and more affordable services (without the typical overheads of a firm). Everyday more and more platforms are emerging to help people do this kind of work. They connect people to gigs, streamline the payment and create trust. All great things, right?'+
        'An unprecedented number of independent workers will disrupt the economic landscape. When I say disrupt I don’t mean in the cliché Silicon Valley definition, I mean in the true definition: “To destroy, usually temporarily, the normal continuance or unity…” A shift like this will disrupt incomes and relationships, personal and professional.'
      }
    },
    {
      img: './assets/img/1.jpg',
      title: 'text 4',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/2.jpg',
      title: 'text 5',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/3.jpg',
      title: 'text 6',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/1.jpg',
      title: 'text 7',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/2.jpg',
      title: 'text 8',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/3.jpg',
      title: 'text 9',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/1.jpg',
      title: 'text 10',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/2.jpg',
      title: 'text 11',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/3.jpg',
      title: 'text 12',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/1.jpg',
      title: 'text 13',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/2.jpg',
      title: 'text 14',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/3.jpg',
      title: 'text 15',
      subtitle: 'One more line of text'
    },
    {
      img: './assets/img/1.jpg',
      title: 'text 16',
      subtitle: 'One more line of text'
    }
  ];
  sideNave = [
    {
      title: 'AWS Global Infrastructure'
    },{
      title: 'Setting Federation'
    },{
      title: 'Encrypting EBS Store'
    },{
      title: 'Creating Public S3'
    }
  ];

  constructor() { }

  getMaterial(){
    return this.arr;
  }
  getCurrentMaterial(i){
    return this.arr[i];
  }
  getMaterialPagination(length){

    let max = length + 3, arr = [];

    max > this.arr.length ? max = this.arr.length : null;

    for(let i = 0; i < max; i++){
      arr.push(this.arr[i])
    }
    return arr

  }

  getMaterialFromSidenav(){
    return this.sideNave
  }


  
}
