$(window).on('load', function () {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded'); 
});

/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */


$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	$('.button-nav').click(function(){
		$(this).toggleClass('active'), 
		$('.main-nav-list').slideToggle(); 
		return false;
	});
	
	/* components */
	
	/*
	
	if($('.styled').length) {
		$('.styled').styler();
	};
	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margin  : 10,
			padding  : 10
		});
	};
	if($('.slick-slider').length) {
		$('.slick-slider').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				  }
				},
				{
				  breakpoint: 600,
				  settings: "unslick"
				}				
			]
		});
	};
	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"x",
			theme:"dark-thin",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};
	
	*/
	
	/* components */

	function slider(){
		$('.slider').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 3,
			arrows: false,
			slidesToScroll: 1,
			responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true
				  }
				},
				{
				  breakpoint: 550,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true
				  }
				}				
			]
		});

		$('.pagination .arrow-prev').click(function(){
		  $('.slider').slick('slickPrev');
		})

		$('.pagination .arrow-next').click(function(){
		  $('.slider').slick('slickNext');
		})
	}


	
	if($('.slider').length) {
		slider();
	};


	function sliderTabs(){
		$('.tabs .tab').on('click', function() {

			var tab_id = $(this).attr('data-tab');

			$('.tabs .tab').removeClass('active');
			$('.tab-content').removeClass('active');

			$(this).addClass('active');
			$("#"+tab_id).addClass('active');
			$('.slider').slick('setPosition', 0);
		});
	}
	sliderTabs();

	$('.search-js .icon').on('click', function() {

		$(this).closest('.search-js').find('.icon').toggleClass('active');
		$(this).closest('.search-js').find('input').toggleClass('opened');
		$(this).closest('.search-js').find('input').focus();

	});


	$('body').on('click', '.open-menu-js', function(){
	    $('.main-menu').addClass('opened');
	    $('.overlay-menu').addClass('visible');
	    $('body').css('overflow', 'hidden');
	});

	$('body').on('click', '.close-menu-js', function(){
	    $('.main-menu').removeClass('opened');
	    $('.overlay-menu').removeClass('visible');
	    $('body').css('overflow', 'auto');
	});

	$('body').on('click', '.overlay-menu', function(){
	    $('.main-menu').removeClass('opened');
	    $(this).removeClass('visible');
	    $('body').css('overflow', 'auto');
	});

	$('.accordion-title.active').closest('.accordion__item').find('.accordion-content').show();

	$('.accordion-js').on('click', '.accordion-title', function(){
		var thiscontent = $(this).closest('.accordion__item').find('.accordion-content');
	    $('.accordion-title').removeClass('active');
	    $(this).addClass('active');

	    $('.accordion-content').not(thiscontent).slideUp();
	    $(thiscontent).slideToggle();
	});


	$(document).mouseup(function(e) {
	    toggleDrop(e);
	});

	function toggleDrop(e){
		var drop = $('.drop-js');
		if (!drop.is(e.target) && drop.has(e.target).length === 0) 
	    {
	        $(drop).removeClass('open');
	    }
	}

	$("body").on("click", ".drop-js .field", function() {
		$('.drop-js').removeClass('open');
	    $(this).closest('.drop-js').addClass('open');
	});
 	
 	var allDrops = $('.drop-js');
 	//take all drops and add first option in field-val
 	allDrops.each(function(){
 		$(this).find(".field-val").html($(this).find('ul li.selected').html());
 	});

 	//click on select option
	$("body").on("click", ".drop-js ul li", function() {
		$('.drop-js').removeClass('open');
	    $(this).closest('.drop-js').find('ul li').removeClass('selected');
	    $(this).addClass('selected');
	    $(this).closest(".drop-js").find('.field-val').html($(this).html());
	});	
	
});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);



